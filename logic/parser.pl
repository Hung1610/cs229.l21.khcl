%Nam thì cao. S -> NP VP, NP -> NNP, NNP -> nam, VP -> VB JJ, VB -> thì, JJ -> cao.
%cao(c_Nam).
%Nam có một con mèo. S -> NP VP, .., VP -> VB NP, VB -> có, NP -> DET NP, NP -> UN NN, UN -> con, NN -> mèo
%tồn_tại(X, có(c_Nam, X)).
%Nam có một cây bút và một quyển sách. S -> NP VP, ..., VP -> .., NP -> NP1 CC NP.
%-tồn_tại(X, có(c_Nam, X) & cây_bút(X)) & tồn_tại(Y, có(c_Nam, Y) & quyển_sách(Y)).
%-tồn_tại(X, tồn_tại(Y, có(c_Nam, Y) & quyển_sách(Y) & có(c_Nam, X) & cây_bút(X))). 
%Mỗi học sinh có một cây bút và một quyển sách. S -> NP VP, NP -> DET NP, NP -> NN, NN -> học_sinh.
%-với_mọi(X, tồn_tại(Y, tồn_tại(Z, học_sinh(X) => (cây_bút(Y) & có(X,Y)) & (quyển_sách(Z) & %có(X,Z)))).
%-với_mọi(X, tồn_tại(Y, học_sinh(X) => (cây_bút(Y) & có(X,Y))) & tồn_tại(Z, học_sinh(X) =>(quyển_sách(Z) & có(X,Z))).
%-với_mọi(X, học_sinh(X) => (tồn_tại(Y, cây_bút(Y) & có(X,Y)) & tồn_tại(Z, quyển_sách(Z) & có(X,Z)))).

s(NP@VP) --> np(NP), vp(VP).
np(N) --> nnp(N).
vp(V@J) --> vb(V), jj(J).
nnp(lambda(P,P@c_Nam)) -->[nam].
nnp(lambda(P,P@c_Minh)) -->[minh].
vb(lambda(P,P)) -->[thì].
jj(lambda(P, cao(P))) --> [cao].
jj(lambda(P, thấp(P))) --> [thấp].










