s(NP@VP) --> np(NP), vp(VP).
np(N) --> nnp(N).
np(N) --> nn(N).
np(N1@N2) --> nn(N1), nnp(N2).
vp(V@J) --> vb(V), jj(J).
vp(V@N) --> vb(V), np(N).
nnp(lambda(P, drs([c_Nam],[]) x P@c_Nam)) -->[nam].
nnp(lambda(P, drs([c_Minh],[]) x P@c_Minh)) -->[minh].
vb(lambda(P,P)) -->[thì];[là].
jj(lambda(P, drs([],[cao(P)]))) --> [cao].
jj(lambda(P, drs([],[thấp(P)]))) --> [thấp].
nn(lambda(P, drs([], [học_sinh(P)]))) --> [học, sinh].
nn(lambda(P, lambda(X, P@ lambda(Y, drs([],[bạn(X,Y)]))))) --> [bạn].