# DO AN: CS229.L21.KHCL Ngu nghia hoc tinh toan
## Step 1: consult files
```
consult('/Users/rockship/src/learn/cs229.l21.khcl/logic_B/pheptoan.pl'),
consult('/Users/rockship/src/learn/cs229.l21.khcl/logic_B/lambda.pl'),
consult('/Users/rockship/src/learn/cs229.l21.khcl/logic_B/kethop.pl'),
consult('/Users/rockship/src/learn/cs229.l21.khcl/logic_B/tuvung.pl'),
consult('/Users/rockship/src/learn/cs229.l21.khcl/logic_B/drs2logic.pl'),
consult('/Users/rockship/src/learn/cs229.l21.khcl/logic_B/model.pl'),
consult('/Users/rockship/src/learn/cs229.l21.khcl/logic_B/parser.pl').
```

## Question needed to answer
```
/*
    Question:
        Q/A
        how does john felt? -> panic -> done
        how is john? -> lost -> done
        Who help john? -> sherlock -> done
        who does marie married? -> john -> done
        who marie look for? -> john -> done
        Who give john a gift? -> marie -> done
        what inside the box? -> letter -> done
        where marie move to? -> letter -> done
        YES/NO
        Is sherlock a friend of john? -> false -> done
        Is marie a significant other of john? -> yes -> done
        Is marie a friend of john? -> yes -> done
        Is marie a friend of sherlock? -> false -> done
        Does marie move to london? -> yes -> done
        Does marie and john have a son? -> yes -> done
        Did john marry marie? -> yes -> done
        Did john help sherlock? -> false -> done
*/
```

## input
```
Q&A
findall(How, (dịchvănbản(drs([], []), [[how, does, john, felt]], R), drs2logic(R, KQ), KQ=lambda(How,C),C),L), list_to_set(L,S).
findall(Who, (dịchvănbản(drs([], []), [[who, help, john]], R), drs2logic(R, KQ), KQ=lambda(Who,C),C),L), list_to_set(L,S).
findall(Who, (dịchvănbản(drs([], []), [[who, does, marie, married]], R), drs2logic(R, KQ), KQ=lambda(Who,C),C),L), list_to_set(L,S).
findall(Who, (dịchvănbản(drs([], []), [[who, marie, look, for]], R), drs2logic(R, KQ), KQ=lambda(Who,C),C),L), list_to_set(L,S).
findall(Who, (dịchvănbản(drs([], []), [[who, give, john, a, gift]], R), drs2logic(R, KQ), KQ=lambda(Who,C),C),L), list_to_set(L,S).
findall(What, (dịchvănbản(drs([], []), [[what, did, marie, give, john]], R), drs2logic(R, KQ), KQ=lambda(What,C),C),L), list_to_set(L,S).
findall(What, (dịchvănbản(drs([], []), [[what, inside, the, box]], R), drs2logic(R, KQ), KQ=lambda(What,C),C),L), list_to_set(L,S).
findall(Where, (dịchvănbản(drs([], []), [[where, is, marie, moving, to]], R), drs2logic(R, KQ), KQ=lambda(Where,C),C),L), list_to_set(L,S).
YES/NO
findall(Wdt, (dịchvănbản(drs([], []), [[is, sherlock, a, friend, of, john]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[is, marie, a, significant, other, of, john]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[is, marie, a, friend, of, john]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[is, marie, a, friend, of, sherlock]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[does, marie, move, to, london]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[does, marie, and, john, have ,a ,son]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[did, john, marry, marie]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[did, john, help, sherlock]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
findall(Wdt, (dịchvănbản(drs([], []), [[is, john, lost]], R), drs2logic(R, KQ), KQ=lambda(Wdt,C),C),L), list_to_set(L,S).
```