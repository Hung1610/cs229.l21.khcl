hằng(X):- từ_vựng(X, 0).
hằng(c_gift).
hằng(c_son).
hằng(c_letter).
hằng(c_box).
hằng(c_panic).
hằng(c_lost).

% truth
gift(c_gift).
son(c_son).
letter(c_letter).
box(c_box).

panic(c_panic).
panic(john).

lost(c_lost).
lost(john).

grow_up(john).

% relationship
feeling(john, c_panic).

reunite(marie, john).
reunite(john, marie).

help(sherlock, john).

have(marie, c_son).
have(marie, c_gift).
have(marie, c_box).
have(marie, c_letter).
have(john, c_son).

move_to(marie, london).

meet(john, sherlock).
meet(sherlock, john).
meet(john, marie).
meet(marie, john).

befriend(john, sherlock).
befriend(john, marie).
befriend(marie, john).

found(marie, john).

give(marie, john, c_gift).

marry(john, marie).
marry(marie, john).

equal(c_gift, c_box).
equal(c_box, c_gift).

contain(c_gift, c_letter).

% conditional relationship
look_for(X, Y):-found(X,Y).
friend(X, Y):-
    befriend(X, Y),
    befriend(Y, X)
.
significant_other(X, Y):-
    marry(X, Y)
.
inside(X, Y):-
    equal(X, Z),
    contain(Z, Y)
.