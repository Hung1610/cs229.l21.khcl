/*
    Question:
        Q/A
        how does john felt? -> panic -> done
        how is john? -> lost -> done
        Who help john? -> sherlock -> done
        who does marie married? -> john -> done
        who marie look for? -> john -> done
        Who give john a gift? -> marie -> done
        what inside the box? -> letter -> done
        YES/NO
        Is sherlock a friend of john? -> yes -> done
        Is marie a significant other of john? -> yes -> done
        Is marie a friend of john? -> yes -> done
        Is marie a friend of sherlock? -> false -> done
        Does marie move to london? -> yes -> done
        Does marie have a son? -> yes -> done
        Does john marry marie? -> yes -> done
        Does john help sherlock? -> false -> done
*/

s(N@V) --> np(N), vp(V).
s(N@J) --> np(N), jj(J).

s(W@S) --> wdt(W), s(S).

s(W@V) --> wp(W), vp(V).
s(W@V) --> vp(V), wp(W).
s(W@V) --> wp(W), vp1(V).
s(W@V) --> vp1(V), wp(W).
s(W@S) --> vp(S), wp1(W).
s(W@V) --> wrb(W), vp(V).

np(N) --> prp(N).
np(N) --> nnp(N).
np(N) --> nn(N).
np(D@N) --> det(D), np(N).
np((C@N1)@N2) --> np1(N1), cc(C), np(N2).

np1(N) --> prp(N).
np1(N) --> nnp(N).
np1(N) --> nn(N).
np1(D@N) --> det(D), np(N).

np2(N) --> prp(N).
np2(N) --> nnp(N).
np2(N) --> nn(N).
np2(D@N) --> det(D), np(N).

vp1((V@N2)@N3) --> vb(V), np1(N2), np2(N3).
vp1((V@N2)@N3) --> np1(N2), vb1(V), np2(N3).
vp(V@N) --> vb(V), np(N).
vp(V@N) --> np(N), vb1(V).
vp(J@N) --> jj(J), np(N).
vp(J@N) --> np(N), jj(J).
vp(V) --> vb(V).

nnp(lambda(P, drs([(john, người)],[]) x (P@john))) --> [john].
nnp(lambda(P, drs([(marie, người)],[]) x (P@marie))) --> [marie].
nnp(lambda(P, drs([(sherlock, người)],[]) x (P@sherlock))) --> [sherlock].
nnp(lambda(P, drs([(london, nơi)],[]) x (P@london))) --> [london].

% rule equivalent to data
% NN
nn(lambda(P, drs([], [gift(P)]))) --> [gift].
nn(lambda(P, drs([], [box(P)]))) --> [box].
nn(lambda(P, drs([], [son(P)]))) --> [son].
nn(lambda(P, drs([], [panic(P)]))) --> [panic].
nn(lambda(P, drs([], [letter(P)]))) --> [letter].

% VBD
vb(lambda(P, P)) -->[is]; [are]; [were]; [of].

vb(lambda(P, drs([], [lost(P)]))) --> [lost].
vb(lambda(P, drs([], [panic(P)]))) --> [panic].
% formular 2
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [reunite(X, Y)]))))) --> [reunite]; [reunites]; [reunited]; [reunite, with]; [reunited, with].
% VBZ
% formular 2
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [have(X, Y)]))))) --> [have]; [has]; [had].
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [help(X, Y)]))))) --> [help]; [helps]; [helping]; [helped].
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [befriend(X, Y)]))))) --> [befriend], [befriends].
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [move_to(X, Y)]))))) --> [move, to]; [moves, to]; [moving, to]; [moved, to].
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [contain(X, Y)]))))) --> [contain]; [contains]; [contained]; [containing].
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [meet(X, Y)]))))) --> [meet]; [meets]; [meeting]; [met].
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [marry(X, Y)]))))) --> [marry]; [marries]; [marrying]; [married]; [significant, other, of].
% formular 3
vb(lambda(P, lambda(Q, lambda(X, P@ lambda(Y, Q@ lambda(Z, drs([], [give(X, Y, Z)]))))))) --> [give]; [giving]; [gave].
% VB rules beyound data knowledge
vb(lambda(P, lambda(X, P@ lambda(Y, drs([], [look_for(X, Y)]))))) --> [look, for]; [looks, for]; [looking, for]; [looked, for].

% inverted VB
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [look_for(Y, X)]))))) --> [look, for]; [looks, for]; [looking, for]; [looked, for].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [have(Y, X)]))))) --> [have]; [has]; [had].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [feeling(Y, X)]))))) --> [feel]; [feeling]; [felt].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [help(Y, X)]))))) --> [help]; [helps]; [helping]; [helped].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [befriend(Y, X)]))))) --> [befriend], [befriends].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [move_to(Y, X)]))))) --> [move, to]; [moves, to]; [moving, to]; [moved, to].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [contain(Y, X)]))))) --> [contain]; [contains]; [contained]; [containing].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [meet(Y, X)]))))) --> [meet]; [meets]; [meeting]; [met].
vb1(lambda(P, lambda(X, P@ lambda(Y, drs([], [marry(Y, X)]))))) --> [marry]; [marries]; [marrying]; [married]; [marry, to]; [marries, to]; [marrying, to]; [married, to].

vb1(lambda(P, lambda(Q, lambda(X, P@ lambda(Y, Q@ lambda(Z, drs([], [give(Y, Z, X)]))))))) --> [give]; [giving]; [gave].

% JJ
jj(D@J) --> det1(D), jj(J).
jj(lambda(P, lambda(X, P@ lambda(Y, drs([], [found(X, Y)]))))) --> [found].
jj(lambda(P, drs([], [grow_up(P)]))) --> [grow, up]; [grows, up]; [growing, up]; [grew, up].
jj(lambda(P, lambda(X, P@ lambda(Y, drs([], [equal(X, Y)]))))) --> [equal].
% JJ rules beyound data knowledge
jj(lambda(P, lambda(X, P@ lambda(Y, drs([], [inside(Y, X)]))))) --> [inside].
jj(lambda(P, lambda(X, P@ lambda(Y, drs([], [friend(X, Y)]))))) --> [friend, of]; [friend, with]; [friend].
jj(lambda(P, lambda(X, P@ lambda(Y, drs([], [significant_other(X, Y)]))))) --> [significant, other]; [significant, other, of].

det(lambda(P, lambda(Q, (drs([(X, _)], []) x (P@X)) x (Q@X)))) --> [a]; [an]; [the].
det1(lambda(P, P)) --> [a]; [an]; [the].

cc(lambda(P, lambda(Q, lambda(X, (P@X) x (Q@X))))) --> [and]; [together, with].

% wh- question rules
prp(lambda(P, lambda(X, drs([(X, _)], []) x (P@X)))) --> [what].

wp(lambda(P, lambda(X, drs([(X, người)], []) x (P@X)))) --> [who]; [who, does]; [who, is].
wp(lambda(P, lambda(X, drs([(X, nơi)], []) x (P@X)))) --> [where]; [where, does]; [where, is].
wp(lambda(P, lambda(X, drs([(X, _)], []) x (P@X)))) --> [what, does]; [what, do]; [what, did]; [what].

wrb(lambda(P, lambda(X, drs([(X, _)], []) x (P@X)))) --> [how]; [how, does]; [how, do]; [how, is].

wdt(lambda(P, lambda(_, P))) --> [does]; [did]; [do]; [is].
