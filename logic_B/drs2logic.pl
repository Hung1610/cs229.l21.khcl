drs2logic(A,A):-A=..[R|_], từ_vựng(R,_).
drs2logic(A,A):-A=..[=|_].

drs2logic(drs([],[drs([(X,_)],A) => B|T]), với_mọi(X, (Ra => Rb) & Rt)):-drs2logic(A,Ra), drs2logic(B, Rb), drs2logic(T, Rt).
drs2logic(drs([],[drs([(X,_)|TX],A) => B|T]), với_mọi(X, R)):-drs2logic(drs([],[drs(TX,A) => B|T]), R).

drs2logic(drs([],[A]), R):-drs2logic(A,R).
drs2logic(drs([],[A|T]), Ra & Rt):- drs2logic(A,Ra), drs2logic(drs([],T),Rt).
drs2logic(drs([(A,_)],B), tồn_tại(A, R)):-drs2logic(drs([],B), R).
drs2logic(drs([(A,_)|T], B), tồn_tại(A, R)):-drs2logic(drs(T,B), R).
drs2logic(lambda(X,A), lambda(X,R)):-drs2logic(A,R).