kếthợp(drs(D,C1),drs(D,C2)):-
    kếthợp(C1,C2).

kếthợp(alpha(X, drs(D,C1)), alpha(X, drs(D,C2))):-
    kếthợp(C1,C2).
 
kếthợp(B1 x alpha((X,XB), B2), drs(D3,C3)):-
    kếthợp(B1,drs(D1,C1)),
    kếthợp(B2,drs(D2,C2)),
    append(D2,D1,D3t),
    sởchỉ(D1, (X,XB), (V,_)),
    append(C1, [X=V], C1t),
    append(C1t,C2,C3t),
    list_to_set(D3t, D3),
    list_to_set(C3t, C3).

kếthợp(B1 x B2,drs(D3,C3)):-
    kếthợp(B1,drs(D1,C1)),
    kếthợp(B2,drs(D2,C2)),
    append(D2,D1,D3t),
    append(C1,C2,C3t),
    list_to_set(D3t, D3),
    list_to_set(C3t, C3).
 
kếthợp([B1 => B2|C1],[B3 => B4|C2]):-
    !, kếthợp(B1,B3), kếthợp(B2,B4),
    kếthợp(C1,C2).
kếthợp([B1 v B2|C1],[B3 v B4|C2]):-
    !,kếthợp(B1,B3),kếthợp(B2,B4),
    kếthợp(C1,C2).
 
kếthợp([~ B1|C1], [~ B2|C2]):-
    !,kếthợp(B1,B2),kếthợp(C1,C2).
 
kếthợp([C|C1],[C|C2]):-
    kếthợp(C1,C2).
 
kếthợp([],[]).
 
kếthợpcâu(drs(P,Q), [], drs(P,Q)).
kếthợpcâu(drs(P,Q), [B1|T] ,drs(D3,C3)):- 
    kếthợp(drs(P,Q) x B1, R1),
    kếthợpcâu(R1, T, drs(D3, C3)).
 
kếthợpcâu(lambda(X, drs(P,Q)), [B1|T], lambda(X, drs(D3,C3))):- 
    kếthợp(drs(P,Q) x B1, R1),
    kếthợpcâu(R1, T, drs(D3, C3)).

kếthợpcâu(drs(P,Q), [lambda(X, B1)|T], lambda(X, drs(D3,C3))):- 
    kếthợp(drs(P,Q) x B1, R1),
    kếthợpcâu(R1, T, drs(D3, C3)).

dịchvănbản(A,[],A):-!.
dịchvănbản(P,[S|T],R):-
    s(Rs, S, []),
    biến_đổi_b(Rs, E),
    danhsáchdrs(E, Re),
    kếthợpcâu(P, Re, Rp),
    dịchvănbản(Rp, T, R),
    !.